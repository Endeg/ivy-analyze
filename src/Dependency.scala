import java.io.File

import scala.collection.mutable
import scala.xml._

/**
  * Created by Andrey_Gladyshev
  * on 01.03.2017.
  */
case class Dependency(org: String, name: String, rev: String) {
  def toXml: String = (<dependency/> %
    Attribute(None, "transitive", Text("false"), Null) %
    Attribute(None, "rev", Text(rev), Null) %
    Attribute(None, "name", Text(name), Null) %
    Attribute(None, "org", Text(org), Null)).toString

  def newRev(revToReplace: String): Dependency = new Dependency(org, name, revToReplace)
  def newName(nameToReplace: String): Dependency = new Dependency(org, nameToReplace, rev)
}

object Dependency {

  val deps: mutable.Map[Dependency, mutable.Set[Dependency]] = mutable.Map.empty

  def indexCache(pathToCache: String, filterDependency: Node => Boolean): Unit = {
    for (file <- Io.listTree(new File(pathToCache), _.getName.endsWith(".xml"))) {
      val xml = XML.loadFile(file)
      val ivyModule = xml \\ "ivy-module"
      val dependencies = ivyModule \\ "dependencies" \\ "dependency"

      val info = ivyModule \\ "info"

      val ownerOrg = info \@ "organisation"
      val ownerName = info \@ "module"
      val ownerRev = info \@ "revision"

      val parent = new Dependency(ownerOrg, ownerName, ownerRev)
      Dependency.add(parent)

      for (dependency <- dependencies.filter(filterDependency)) {
        val org = dependency \@ "org"
        val name = dependency \@ "name"
        val rev = dependency \@ "rev"
        Dependency.deps(parent).add(new Dependency(org, name, rev))
      }
    }
  }

  def add(dep: Dependency): Unit = {
    if (!deps.contains(dep)) {
      deps.put(dep, mutable.Set.empty)
    }
  }

  def requiredDependencies(parent: Dependency): Set[Dependency] = {
    if (deps.contains(parent)) {
      (deps(parent).flatMap((x: Dependency) => requiredDependencies(x)).map(x => fixRev(x)) + fixRev(parent)).toSet
    } else {
      Set(fixRev(parent))
    }
  }

  def writeForIvyModule(stringFromResolveLog: Set[String]): Unit = {
    val resolvedDpendencies = stringFromResolveLog.
      map(fromResolveString).
      flatMap(requiredDependencies).
      toStream.
      sortBy(_.toString)

    for (dependency <- resolvedDpendencies) {
      println(dependency.toXml)
    }
  }

  private def fromResolveString(resolveString: String) = {
    val (org, name, rev) = splitProperly(resolveString)
    val parent = new Dependency(org, name, rev)
    parent
  }

  def splitProperly(s: String): (String, String, String) = {
    val orgNameRev = s.split(";")
    val orgName = orgNameRev(0).split("#")
    (orgName(0), orgName(1), orgNameRev(1))
  }

  def fixRev(original: Dependency): Dependency = {

    val workingRev = "(working\\@.*)".r

    original match {
      case Dependency("javax.ws.rs", "javax.ws.rs-api", _) => original.newRev("${javax.rest-api.version}")
      case Dependency("javax.annotation", "javax.annotation-api", _) => original.newRev("${javax.annotation.version}")
      case Dependency("com.fasterxml.jackson.core", _, _) => original.newRev("${jackson.version}")
      case Dependency("com.fasterxml.jackson.dataformat", _, _) => original.newRev("${jackson.version}")
      case Dependency("com.fasterxml.jackson.jaxrs", _, _) => original.newRev("${jackson.version}")
      case Dependency("org.glassfish.hk2", _, _) => original.newRev("${hk2.api.version}")
      case Dependency("org.glassfish.hk2.external", "cglib", _) => original.newRev("2.2.0-b23")
      case Dependency("org.glassfish.hk2.external", _, _) => original.newRev("${hk2.api.version}")
      case Dependency("javax.persistence", "persistence-api", _) => original.newRev("${persistence-api.version}")
      case Dependency("javax.validation", "validation-api", _) => original.newRev("${validation-api.version}")
      case Dependency("org.osgi", "org.osgi.core", _) => original.newRev("4.2.0")
      case Dependency("javax.enterprise", "cdi-api", _) => original.newRev("${cdi-api.version}")
      case Dependency("org.slf4j", _, _) => original.newRev("${slf4j.version}")
      case Dependency("org.glassfish.jersey.core", _, _) => original.newRev("${jersey.version}")
      case Dependency("org.glassfish.jersey.media", _, _) => original.newRev("${jersey.version}")
      case Dependency("org.glassfish.jersey.containers", _, _) => original.newRev("${jersey.version}")
      case Dependency("org.glassfish.jersey.bundles.repackaged", _, _) => original.newRev("${jersey.version}")
      case Dependency("org.javassist", _, _) => original.newRev("${javassist.version}")
      case Dependency("javax.servlet", "servlet-api", "${servlet2.version}") => original.newName("javax.servlet-api")
      case Dependency("javax.inject", "javax.inject", _) => original.newRev("1")
      case Dependency("com.fasterxml.jackson.jaxrs", _, _) => original.newRev("2.8.7")
      case Dependency("org.glassfish.main.ejb", _, _) => original.newRev("4.1.1")
      case Dependency("org.glassfish.main.common", _, _) => original.newRev("4.1.1")
      case Dependency("io.swagger", _, _) => original.newRev("${swagger.version}")
      case Dependency("org.glassfish.jersey.ext.cdi", _, _) => original.newRev("${jersey.version}")
      case Dependency("org.glassfish.jersey.ext", _, _) => original.newRev("${jersey.version}")
      case Dependency("javax.servlet", "javax.servlet-api", _) => original.newRev("${servlet3.version}")
      case Dependency("com.google.guava", "guava", _) => original.newRev("${guava.version}")
      case Dependency("org.jvnet.mimepull", "mimepull", _) => original.newRev("${mimepull.version}")

      case Dependency(_, _, workingRev(rev)) => throw new IllegalArgumentException(s"Cannot find replacement for ${original.toXml}: working revision $rev provided.")
      case _ => original
    }
  }
}