

/**
  * Created by Andrey_Gladyshev
  * on 28.02.2017.
  */
object Runner {

  def main(args: Array[String]) = {
    val PATH_TO_CACHE = "e:\\Cache\\ivy\\local"
    val PROBLEMATIC_DEPENDENCY = Set(
      "org.glassfish.jersey.containers#jersey-container-servlet;2.25.1",
      "org.glassfish.jersey.ext#jersey-mvc;2.25.1",
      "org.glassfish.jersey.ext#jersey-mvc-jsp;2.25.1",
      "org.glassfish.jersey.media#jersey-media-multipart;2.25.1",
      "org.glassfish.jersey.media#jersey-media-json-jackson;2.25.1",
      "org.glassfish.jersey.ext.cdi#jersey-cdi1x-transaction;2.25.1",
      "org.glassfish.jersey.ext.cdi#jersey-cdi1x-servlet;2.25.1",
      "org.glassfish.jersey.ext.cdi#jersey-cdi1x;2.25.1",
      "org.glassfish.jersey.containers.glassfish#jersey-gf-ejb;2.25.1",
      "io.swagger#swagger-core;1.5.12",
      "io.swagger#swagger-jersey2-jaxrs;1.5.12"
    )

    Dependency.indexCache(PATH_TO_CACHE,
      x => !(x \@ "conf").contains("test") && !(x \@ "conf").contains("optional") &&
        (x \@ "name") != "junit" && (x \@ "name") != "jmockit" &&
        (x \@ "org") != "javax.ejb" && (x \@ "name") != "javax.ejb-api" &&
        (x \@ "org") != "javax.servlet" && (x \@ "name") != "servlet-api"
    )
    Dependency.writeForIvyModule(PROBLEMATIC_DEPENDENCY)
  }
}
