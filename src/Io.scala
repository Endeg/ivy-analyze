import java.io.File

/**
  * Created by Endeg on 19.01.2017.
  */
object Io {
  def listTree(f: File, filterFunction: (File) => Boolean): Stream[File] = {
    if (f.isDirectory) {
      f.listFiles.toStream.flatMap(listTree(_, filterFunction))
    } else {
      Stream.apply(f).filter(filterFunction)
    }
  }
}
